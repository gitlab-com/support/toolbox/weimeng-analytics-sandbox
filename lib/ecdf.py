import numpy as np

def ecdf(data):
    """Compute empirical cumulative distribution function for a one-dimensional array of measurements."""
    n = len(data)
    x = np.sort(data)
    y = np.arange(1, n + 1)

    return x,y
