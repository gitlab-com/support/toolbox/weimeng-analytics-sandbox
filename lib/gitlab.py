import pandas as pd

class SupportTimeSegmenter:
    def __init__(self, df, hour_feature):
        self.df = df
        self.hour_column = df[hour_feature]

    def get_segment(self, region, segment):
        regions = {
            'amer': {
                'all': self.__amer_all(),
                'early': self.__amer_early(),
                'middle': self.__amer_middle(),
                'late': self.__amer_late(),
            },
            'apac': {
                'all': self.__apac_all(),
                'early': self.__apac_early(),
                'middle': self.__apac_middle(),
                'late': self.__apac_late(),
            },
            'emea': {
                'all': self.__emea_all(),
                'early': self.__emea_early(),
                'late': self.__emea_late(),
            },
        }

        selected_region = regions.get(region)
        if not selected_region:
            raise ValueError("Region '" + region + "' not found")

        mask = selected_region.get(segment, pd.Series([]))
        if mask.empty:
            raise ValueError("Segment '" + segment + "' not found")

        return self.df[mask]

    def __amer_all(self):
        return (self.hour_column >= 13) | (self.hour_column < 1)

    def __amer_early(self):
        return (self.hour_column >= 13) & (self.hour_column < 17)

    def __amer_middle(self):
        return (self.hour_column >= 17) & (self.hour_column < 21)

    def __amer_late(self):
        return (self.hour_column >= 21) | (self.hour_column < 1)

    def __apac_all(self):
        return (self.hour_column >= 23) | (self.hour_column < 12)

    def __apac_early(self):
        return (self.hour_column >= 23) | (self.hour_column < 2)

    def __apac_middle(self):
        return (self.hour_column >= 2) & (self.hour_column < 6)

    def __apac_late(self):
        return (self.hour_column >= 6) & (self.hour_column < 12)

    def __emea_all(self):
        return (self.hour_column >= 7) & (self.hour_column < 17)

    def __emea_early(self):
        return (self.hour_column >= 7) & (self.hour_column < 13)

    def __emea_late(self):
        return (self.hour_column >= 13) & (self.hour_column < 17)
