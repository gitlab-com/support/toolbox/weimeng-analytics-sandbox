import datetime
import pandas as pd
import pytz
import yaml

support_team_files = {
  '2020-09-30': '../_inputs/support-team_2020-09-30.yaml',
  '2020-10-31': '../_inputs/support-team_2020-10-31.yaml',
  '2020-11-30': '../_inputs/support-team_2020-11-30.yaml',
  '2020-12-31': '../_inputs/support-team_2020-12-31.yaml',
  '2021-01-31': '../_inputs/support-team_2021-01-31.yaml',
  '2021-02-28': '../_inputs/support-team_2021-02-28.yaml'
}

support_team_timezones = pd.read_csv('../_inputs/support-team_timezones.csv', index_col='GitLab ID')

availability_cols = ['Month', 'GitLab ID', 'Focus', 'Percentage', 'Timezone']
availability_data = []

for month, filename in support_team_files.items():
    data = yaml.load(open(filename), Loader=yaml.BaseLoader)
    for team_member in data:
        for focus in team_member['focuses']:
            row_data = [
              month,
              team_member['gitlab']['id'],
              focus['name'],
              focus['percentage']
            ]

            try:
                row_data.append(support_team_timezones.loc[int(team_member['gitlab']['id']), 'Timezone'])
            except:
                row_data.append('')

            availability_data.append(row_data)

availability_df = pd.DataFrame(availability_data, columns=availability_cols)

def convert_percentage_str_to_float(string):
    try:
        perc = float(string) / 100
    except:
        perc = float('NaN')

    return perc

# Cast column types
availability_df['Month'] = pd.to_datetime(availability_df['Month'])
availability_df['Percentage'] = availability_df['Percentage'].apply(lambda row: convert_percentage_str_to_float(row))

# Drop NaNs
availability_df = availability_df[availability_df['Percentage'].notna()]

availability_hours_cols = ['Month', 'Focus']
for hour in range(24):
    availability_hours_cols.append('Hour ' + str(hour))

availability_hours_data = []

for _, availability in availability_df.iterrows():
    row_data = [
      availability['Month'],
      availability['Focus']
    ]

    percentage = availability['Percentage']
    timezone = pytz.timezone(availability['Timezone'])

    start_time = timezone.localize(datetime.datetime(2021, 1, 1, 9, 0, 0))
    end_time = timezone.localize(datetime.datetime(2021, 1, 1, 17, 0, 0))
    start_hour_utc = start_time.astimezone(pytz.utc).hour
    end_hour_utc = end_time.astimezone(pytz.utc).hour

    times = [0] * 24

    if end_hour_utc > start_hour_utc:
        for hour in range(start_hour_utc, end_hour_utc):
            times[hour] = percentage
    else:
        for hour in range(start_hour_utc, 24):
            times[hour] = percentage
        for hour in range(0, end_hour_utc):
            times[hour] = percentage

    for time in times:
        row_data.append(time)

    availability_hours_data.append(row_data)

availability_hours_df = pd.DataFrame(availability_hours_data, columns=availability_hours_cols)

total_availability = availability_hours_df.groupby([
    pd.Grouper(key='Focus'),
    pd.Grouper(key='Month', freq='M')
]).sum().reset_index()

total_availability.to_csv(r'team_total_availability.csv', index = False)
