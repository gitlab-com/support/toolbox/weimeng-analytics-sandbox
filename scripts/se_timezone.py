import datetime
import pandas as pd
import pytz
import sys
import yaml

from geopy import geocoders
from tzwhere import tzwhere

filename = sys.argv[1]

# Read support-team.yaml into dataframe
# Download from https://gitlab.com/gitlab-com/support/team/-/blob/master/data/support-team.yaml (internal only)
support_data = yaml.load(open(filename), Loader=yaml.BaseLoader)

df_columns = ['Name', 'Region', 'Timezone']

print(df_columns)

g = geocoders.Nominatim(user_agent='glsupport-nominal-roll')

df_data = []
for member in support_data:
    print(member['name'])

    member_data = [
      member['name'],
      member['region']
    ]

    member_loc = member['location'] + ', ' + member['country']
    geocode_result = g.geocode(member_loc)
    if geocode_result is None:
        print('-- skipped! Unknown location: ' + member_loc)
        continue

    _, (lat, lng) = geocode_result

    timezone = tzwhere.tzwhere().tzNameAt(lat, lng)
    member_data.append(timezone)

    df_data.append(member_data)

data = pd.DataFrame(df_data, columns=df_columns)

print(data.head())

data.to_csv(r'se_timezone.csv', index = False)
