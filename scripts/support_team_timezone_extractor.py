import pandas as pd
import sys
import yaml

filename = sys.argv[1]

# Read support-team.yaml into dataframe
support_data = yaml.load(open(filename), Loader=yaml.BaseLoader)

df_columns = ['GitLab ID', 'Timezone']
df_data = []

for member in support_data:
    member_data = [
      member['gitlab']['id'],
      member['tzdata_timezone']
    ]

    df_data.append(member_data)

data = pd.DataFrame(df_data, columns=df_columns)
data.to_csv(r'se_timezone.csv', index = False)
