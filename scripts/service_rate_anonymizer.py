import pandas as pd
import sys
import yaml

from hashlib import sha256

filename = sys.argv[1]

data = pd.read_csv(filename)

data['Updater ID'] = data['Updater ID'].apply(lambda row: sha256(str(row).encode('utf-8')).hexdigest())

data.to_csv(r'service_rate_anonymized.csv', index = False)
