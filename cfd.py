import matplotlib.dates as mdates
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import sys

filename = sys.argv[1]

# Read CSV into dataframe
# Export CSV from https://gitlab.zendesk.com/explore/#/pivot-table/connection/8687831/query/56581612
data = pd.read_csv(filename)

# Cast columns
data['SLA update - Timestamp'] = pd.to_datetime(data['SLA update - Timestamp'])

data['(WM) SLA created - Timestamp'] = data['(WM) SLA created - Timestamp'].apply(lambda row: row[:19])
data['(WM) SLA created - Timestamp'] = pd.to_datetime(data['(WM) SLA created - Timestamp'])

data['(WM) SLA completed - Timestamp'] = data['(WM) SLA completed - Timestamp'].apply(lambda row: row[:19])
data['(WM) SLA completed - Timestamp'] = pd.to_datetime(data['(WM) SLA completed - Timestamp'])

# print(data.head())
# print(data.dtypes)
# print(data.describe())
# print(data.groupby(['Ticket form', 'SLA metric', 'Preferred Region for Support'])['(WM) SLA achieved'].describe())



data['SLA created - POSIX'] = data['(WM) SLA created - Timestamp'].apply(lambda row: row.timestamp())
data['SLA update - POSIX'] = data['SLA update - Timestamp'].apply(lambda row: row.timestamp())
data['SLA completed - POSIX'] = data['(WM) SLA completed - Timestamp'].apply(lambda row: row.timestamp())

data['SLA expiry - POSIX'] = data['SLA created - POSIX'] + (data['SLA metric target time (hrs)'] * 60 * 60)

only_sm = data['Ticket form'] == 'Self-Managed'

data_temp = data[only_sm]
# print(data_temp.head())
data_temp = data_temp[data_temp['SLA metric status'] == 'Completed']

date_mask = (data_temp['(WM) SLA created - Timestamp'] >= '2021-02-25') & (data_temp['(WM) SLA created - Timestamp'] < '2021-02-27')
data_temp = data_temp[date_mask]
# print(data_temp.head())
data_by_sla_start = data_temp.sort_values('SLA created - POSIX')

# prev_row = 0

fig, ax = plt.subplots()

# first_item_time = data_by_sla_start['(WM) SLA created - Timestamp'].iloc[0]

extern_index = 1

for _index, row in data_by_sla_start.iterrows():
    if (row['(WM) SLA achieved'] == 1):
      ax.broken_barh(
        [
          (
            int(row['SLA created - POSIX']),
            int(row['SLA completed - POSIX'] - row['SLA created - POSIX'])
          ),
          (
            int(row['SLA completed - POSIX']),
            int(row['SLA expiry - POSIX'] - row['SLA completed - POSIX'])
          )
        ],
        (extern_index * 6, 5),
        facecolors=('green', 'lightblue')
      )
    else:
      ax.broken_barh(
        [
          (
            int(row['SLA created - POSIX']),
            int(row['SLA update - POSIX'] - row['SLA created - POSIX'])
          ),
          (
            int(row['SLA update - POSIX']),
            int(row['SLA completed - POSIX'] - row['SLA update - POSIX'])
          )
        ],
        (extern_index * 6, 5),
        facecolors=('green', 'red')
      )

    extern_index += 1


# ax.xaxis_date()
# date_form = mdates.DateFormatter("%m-%d")
# ax.xaxis.set_major_formatter(date_form)

xtick_locations = ax.xaxis.get_majorticklocs()
xtick_new_labels = [
    '{0:04n}-{1:02n}-{2:02n}\n{3:02n}:{4:02n}:{5:02n}'.format(
    t.year,t.month,t.day,t.hour,t.minute,t.second) for t in
    [pd.Timestamp(tick,unit='s') for tick in xtick_locations]]
plt.xticks(xtick_locations,xtick_new_labels,rotation=45)

plt.show()
