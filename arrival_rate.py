import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import sys

from lib.ecdf import ecdf

filename = sys.argv[1]

# Read CSV into dataframe
data = pd.read_csv(filename)

data['Updates'].fillna(value=0, inplace=True)

print(data.shape)
print(data.describe())

date_mask = data['Update - Year'] == 2021
data = data[date_mask]

# Filter out weekends
weekday_mask = (data['Update - Weekday'] != 'Saturday') & (data['Update - Weekday'] != 'Sunday')
weekday_data = data[weekday_mask]

print(weekday_data.shape)
print(weekday_data.describe())

# https://about.gitlab.com/support/#effect-on-support-hours-if-a-preferred-region-for-support-is-chosen
# AMER: UTC 13:00 to 01:00
# APAC: UTC 22:00 to 11:00
# EMEA: UTC 07:00 to 17:00

hours = data['Update - Hour']
amer_mask = (hours >= 13) | (hours < 1)
apac_mask = (hours >= 22) | (hours < 11)
emea_mask = (hours >= 7 ) & (hours < 17)

amer_data = weekday_data[amer_mask]
apac_data = weekday_data[apac_mask]
emea_data = weekday_data[emea_mask]

#
# Output
#

titles = ['Global', 'AMER', 'APAC', 'EMEA']
arrival_times = [weekday_data, amer_data, apac_data, emea_data]

for i, arrivals in enumerate(arrival_times):
  arrival_data = arrivals['Updates']
  print(titles[i] + ' average: ' + str(arrival_data.mean()))

fig, ax = plt.subplots(2, 4)

fig.suptitle('Average customer updates per hour for SM with SLA queue (2021-Jan to 2021-Feb)')
fig.tight_layout()

for i, arrivals in enumerate(arrival_times):
  arrival_data = arrivals['Updates']

  ax[0, i].hist(arrival_data, density=True, bins=100)
  ax[0, i].axvline(x=arrival_data.mean(), color='r', label='mean')
  ax[0, i].text(arrival_data.mean() + 1, ax[0, i].get_ylim()[1] * 0.9, 'Mean: ' + str(round(arrival_data.mean(), 2)), color='r')
  ax[0, i].set_title(titles[i])
  ax[0, i].set_xlabel('Customer updates per hour')
  ax[0, i].set_ylabel('PDF')

  ax[1, i]

  data_x, data_y = ecdf(arrival_data)
  data_bootstrap = np.random.poisson(arrival_data.mean(), len(arrival_data))
  data_theor_x, data_theor_y = ecdf(data_bootstrap)

  ax[1, i].plot(data_x, data_y, marker='.', linestyle='none')
  ax[1, i].plot(data_theor_x, data_theor_y)
  ax[1, i].set_xlabel('Customer updates per hour')
  ax[1, i].set_ylabel('CDF')

plt.show()

