import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import sys

from lib.ecdf import ecdf

filename = sys.argv[1]

# Read CSV into dataframe
data = pd.read_csv(filename)

data['Updater ID'] = data['Updater ID'].astype(str)
data['Update - Timestamp'] = pd.to_datetime(data['Update - Timestamp'])

data.sort_values(['Updater ID', 'Update - Timestamp'], inplace=True)

# Calculate time between agent responses
prev_row = pd.Series([], dtype=object)
for i, row in data.iterrows():
    # Skip row if first row or Updater ID changes
    if not (prev_row.empty or (row['Updater ID'] != prev_row['Updater ID'])):
        cycle_time = row['Update - Timestamp'] - prev_row['Update - Timestamp']

        data.at[i, 'Cycle Time (days)'] = cycle_time
        data.at[i, 'Cycle Time (mins)'] = round(cycle_time.total_seconds() / 60)
        data.at[i, 'Cycle Time (hrs)'] = cycle_time.total_seconds() / 60 / 60

    prev_row = row

# Get rid of null data
data_filtered = data[data['Cycle Time (days)'].notna()]

# Get rid of cycle times longer than 8 hours
# Note: 8 hours was chosen as anything longer would go beyond a typical work day
mask = data_filtered['Cycle Time (mins)'] <= 8 * 60
data_filtered = data_filtered[mask]

print(data_filtered.describe())

date_mask = data_filtered['Update - Timestamp'] >= '2021-01-01'
data_filtered = data_filtered[date_mask]

data_filtered['Update - Hour'] = data_filtered['Update - Timestamp'].apply(lambda row: row.hour)

hours = data_filtered['Update - Hour']
amer_mask = (hours >= 13) | (hours < 1)
apac_mask = (hours >= 22) | (hours < 11)
emea_mask = (hours >= 7 ) & (hours < 17)

amer_data = data_filtered[amer_mask]
apac_data = data_filtered[apac_mask]
emea_data = data_filtered[emea_mask]

#
# Output
#

datasets = [data_filtered, amer_data, apac_data, emea_data]
titles = ['Global', 'AMER', 'APAC', 'EMEA']

for i, data in enumerate(datasets):
  print('====================')
  print(f' {titles[i]}')
  print('--------------------')
  print('Average minutes per ticket: ' + str(data['Cycle Time (mins)'].mean()))
  print('Average hours per ticket: ' + str(data['Cycle Time (hrs)'].mean()))
  print('Average tickets per hour: ' + str(1 / data['Cycle Time (hrs)'].mean()))
  print('Summary statistics:')
  print(data.describe())
  print()

fig, ax = plt.subplots(2, 4)
fig.suptitle('Average time between support engineer responses (mins) for SM with SLA queue - 2021-Jan to 2021-Feb')
fig.tight_layout()

cycle_times = [data_filtered['Cycle Time (mins)'], amer_data['Cycle Time (mins)'], apac_data['Cycle Time (mins)'], emea_data['Cycle Time (mins)']]

for i, data in enumerate(cycle_times):
  ax[0, i].hist(data, bins=100, density=True)
  ax[0, i].axvline(x=data.mean(), color='r', label='mean')
  ax[0, i].text(data.mean() + 10, ax[0, i].get_ylim()[1] * 0.9, 'Mean: ' + str(round(data.mean(), 2)), color='r')
  ax[0, i].set_title(titles[i])
  ax[0, i].set_xlabel('Time between SE responses (mins)')
  ax[0, i].set_ylabel('PDF')

  data_x, data_y = ecdf(data)
  data_bootstrap = np.random.exponential(data.mean(), len(data))
  data_theor_x, data_theor_y = ecdf(data_bootstrap)

  ax[1, i].plot(data_x, data_y, marker='.', linestyle='none')
  ax[1, i].plot(data_theor_x, data_theor_y)
  ax[1, i].set_xlabel('Cycle Time (mins)')
  ax[1, i].set_ylabel('CDF')

plt.show()

